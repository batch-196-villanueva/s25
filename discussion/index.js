// console.log("Hello World!");

//JSON
// JSON or Javascript Object Notation is a popular data format for application to communicate with each other.
// JSON may look like a Javascript Object but it is actually a string.

/*
    JSON Syntax

    {
        "key1": "valueA",
        "key2": "valueB",

    }

    Keys are wrapped in curly brace

*/

let sample1 = `
    {
        "name": "Katniss Everdeen",
        "age": 20,
        "address": {
            "city": "Kansas City",
            "state": "Kansas"
        }
    }

`;

console.log(sample1);

// Are we able to turn a JSON into a JS Object?
// JSON.parse() will return the JSON as a JS Object

console.log(JSON.parse(sample1));

// JSON Array
// JSON array is an array of JSON

let sampleArr = `
    [
        {
            "email": "jasonNewsted@gmail.com",
            "password": "iplaybass1234",
            "isAdmin": "false"
        },
        {
            "email": "larsDrums@gmail.com",
            "password": "metallicaMe80",
            "isAdmin": "true"
        }

    ]

`
console.log(sampleArr);

// Can we use Array Methods on a JSON array?
// No. Because JSON is a string.
// So what can we do to be able to add more items/objects into our sampleArr?

let parsedSampleArr = JSON.parse(sampleArr);
console.log(parsedSampleArr)

// Can we now delete the last item in the JSON Array?
console.log(parsedSampleArr.pop());
console.log(parsedSampleArr);

// If for example we need to send this data back to the client/front end, it should be in JSON format


// JSON.parse() does not mutate or update the original JSON
// We can actually turn a JS Object into a JSON
// JSON.stringify() - will stringify JS Objects as JSON


sampleArr = JSON.stringify(parsedSampleArr);
console.log(sampleArr);

// Database (JSON) => Server/API (JSON to JS objet to process) => sent as JSON => client

/*

*/